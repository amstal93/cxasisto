package main

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

func MineThePornDude(b Blacklister) {
	collector := b.GetColly()
	// Instantiate default collector
	c := colly.NewCollector()

	// If the link is behind a review page, visit it and get the link
	r := c.Clone()
	r.OnHTML("a.big-thumb-holder", func(e *colly.HTMLElement) {
		collector.Visit(e.Attr("href"))
	})

	// Search each box containing links
	c.OnHTML("div.category-wrapper", func(e *colly.HTMLElement) {
		e.DOM.Find("a.link.link-analytics").Each(func(_ int, s *goquery.Selection) {
			href, _ := s.Attr("href")
			if strings.HasPrefix(href, "/") {
				// The host is behind a review page
				r.Visit(e.Request.AbsoluteURL(href))
				return
			}
			// Visit the host to remove any redirects and blacklist it.
			collector.Visit(href)
		})
	})
	// Start scraping
	c.Visit("https://theporndude.com/")
}
